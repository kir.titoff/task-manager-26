package ru.t1.ktitov.tm.exception.field;

public final class EmptyEmailException extends AbstractFieldException {

    public EmptyEmailException() {
        super("Error! Email is empty.");
    }

}
