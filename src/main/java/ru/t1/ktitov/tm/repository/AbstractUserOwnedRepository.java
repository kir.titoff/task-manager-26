package ru.t1.ktitov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.IUserOwnedRepository;
import ru.t1.ktitov.tm.model.AbstractUserOwnedModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Nullable
    @Override
    public M add(
            @Nullable final String userId,
            @Nullable final M model
    ) {
        if (userId == null) return null;
        if (model == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull final List<M> models = findAll(userId);
        removeAll(models);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null) return Collections.emptyList();
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator comparator
    ) {
        @NotNull final List<M> result = findAll(userId);
        if (comparator == null) return null;
        result.sort(comparator);
        return result;
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public M findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || id == null) return null;
        return models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (index == null || index < 0) return null;
        return findAll(userId).get(index);
    }

    @Override
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return 0;
        return (int) models.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

    @Nullable
    @Override
    public M removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || index == null) return null;
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M remove(
            @Nullable final String userId,
            @Nullable final M model
    ) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

}
