package ru.t1.ktitov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-change-password";

    @NotNull
    public static final String DESCRIPTION = "Change password of current user";

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        System.out.print("ENTER NEW PASSWORD: ");
        @NotNull final String userId = getAuthService().getUserId();
        @NotNull final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
